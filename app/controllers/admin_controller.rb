class AdminController < ApplicationController
  before_action :authenticate_user!
  before_action :require_admin!

  private

  def require_admin!
    unless current_user.try(:admin)
      redirect_to root_path, flash: { error: "Insufficient rights!" }
    end 
  end
end