class Product < ApplicationRecord
    before_validation :set_asin
    monetize :price_cents

    validates_presence_of :name
    validates :url, url: { no_local: true }

    private 
    def set_asin
        self.asin = Amazon::Asin.from_url(url)
    end
end
