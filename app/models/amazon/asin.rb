module Amazon
    class Asin
        # Return the ASIN from a URL copied and pasted by the user
        # Return false if no ASIN is found
        def self.from_url(amazon_url)
            if amazon_url.match(/\/dp\/(\w{10})(\/|\Z)/)
                # /dp/B0015T963C
                asin = $1
            elsif amazon_url.match(/\/dp\/(\w{10})/)
                # /dp/B0015T963C?
                asin = $1
            elsif amazon_url.match(/\/gp\/\w*?\/(\w{10})(\/|\Z)/)
                # /gp/product/D0015T963C
                asin = $1
            elsif amazon_url.match(/\/gp\/\w*?\/\w*?\/(\w{10})(\/|\Z)/)
                # /gp/product/glance/E0015T963C
                asin = $1
            elsif amazon_url.match(/\/gp\/[\w-]*?\/(\w{10})(\/|\Z)/)
                # /gp/offer-listing/F018Y23P7K
                asin = $1
            elsif amazon_url.match(/\/product-reviews\/(\w{10})(\/|\Z)/)
                # /product-reviews/G018Y23P7K
                asin = $1
            elsif amazon_url.match(/[?&]asin=(\w{10})(&|#|\Z)/i)
                # ?asin=H018Y23P7K
                # &ASIN=H018Y23P7K
                asin = $1
            else
                asin = nil
            end
        end
    end
end