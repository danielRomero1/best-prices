json.extract! user, :id, :name, :model, :manufacturer, :amount, :asin, :created_at, :updated_at
json.url user_url(user, format: :json)
