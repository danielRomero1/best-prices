json.extract! product, :id, :name, :model, :manufacturer, :amount, :asin, :created_at, :updated_at
json.url product_url(product, format: :json)
