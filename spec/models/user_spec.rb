require 'rails_helper'

RSpec.describe User, type: :model do
  subject{ described_class.new(valid_params) }
  
  let(:valid_params){ {email: 'example@example.org', password: '123456', password_confirmation: '123456'} }
  
  it "is valid with valid attributes" do
    is_expected.to be_valid
  end

  describe "#admin" do
    it "is not admin by default" do
      expect(subject.admin).to be_falsey
    end

    it "is admin by force" do
      subject.admin = true
      expect(subject.admin).to be_truthy
    end

    it "can not be empty" do
      subject.admin = nil
      expect(subject.admin).to be_falsey
    end
  end
  
  describe "#email" do
    it "is not valid without a email" do
      subject.email = nil
      is_expected.to_not be_valid
    end
  
    it "is not valid with invalid email" do
      subject.email = 'my_email.com'
      is_expected.to_not be_valid
      expect(subject.errors.messages[:email]).to eq ["is invalid", "is not a valid email"]
    end
  end
end
