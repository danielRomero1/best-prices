require 'rails_helper'

RSpec.describe Amazon::Asin, type: :model do
    describe "#rom_url" do
      context "/dp/... in path" do
        it '/dp/A01LTHM8LG/' do
            url = "https://smile.amazon.com/Programmable-Touchscreen-Thermostat-Geofencing-HomeKit/dp/A01LTHM8LG/ref=s9u_simh_gw_i1?_encoding=UTF8&fpl=fresh&pd_rd_i=B01LTHM8LG&pd_rd_r=21CHQY4CPXGGXZ5G3Q71&pd_rd_w=imw1F&pd_rd_wg=CNLFs&pf_rd_m=ATVPDKIKX0DER&pf_rd_s=desktop-1&pf_rd_r=XDXMZR4E839F0JD45S0F&pf_rd_r=XDXMZR4E839F0JD45S0F&pf_rd_t=36701&pf_rd_p=781f4767-b4d4-466b-8c26-2639359664eb&pf_rd_p=781f4767-b4d4-466b-8c26-2639359664eb&pf_rd_i=desktop"            
            asin = "A01LTHM8LG"
            calculate_asin = described_class.from_url(url)
            expect(calculate_asin).to eq asin
        end

        it '/Kindle-Wireless-Reading-Display-Generation/dp/B0015T963C' do
            url = "http://www.amazon.com/Kindle-Wireless-Reading-Display-Generation/dp/B0015T963C"
            asin = "B0015T963C"
            calculate_asin = described_class.from_url(url)
            expect(calculate_asin).to eq asin
        end

        it '/dp/C0015T963C' do
            url = "http://www.amazon.com/dp/C0015T963C"
            asin = "C0015T963C"
            calculate_asin = described_class.from_url(url)
            expect(calculate_asin).to eq asin
        end

        it '/dp/1497445825/' do
          url = "https://smile.amazon.com/Korean-Made-Simple-beginners-learning/dp/1497445825/ref=sr_1_1?ie=UTF8&qid=1493580746&sr=8-1&keywords=korean+made+simple"
          asin = "1497445825"
          calculate_asin = described_class.from_url(url)
          expect(calculate_asin).to eq asin
        end

        it '/dp/1497445825/' do
          url = "https://www.amazon.es/nuevo-echo-dot-4a-generacion-altavoz-inteligente-con-alexa-antracita/dp/B084DWG2VQ?pd_rd_w=dbpRD&pf_rd_p=1024d140-6029-4658-a964-39fcd650f7a6&pf_rd_r=3D4673K1W0VH49YJ1SW4&pd_rd_r=6e9f10f9-912a-4b78-bede-984c15b6fdec&pd_rd_wg=96j3g&pd_rd_i=B084DWG2VQ&psc=1&ref_=pd_bap_d_rp_23_t"
          asin = "B084DWG2VQ"
          calculate_asin = described_class.from_url(url)
          expect(calculate_asin).to eq asin
        end        
      end

      context "/gp/... in path" do
        it '/gp/product/glance/E0015T963C' do
          url = "http://www.amazon.com/gp/product/glance/E0015T963C"
          asin = "E0015T963C"
          calculate_asin = described_class.from_url(url)
          expect(calculate_asin).to eq asin
        end

        it 'gp/offer-listing/F018Y23P7K/' do
          url = "https://smile.amazon.com/gp/offer-listing/F018Y23P7K/ref=dp_olp_all_mbc?ie=UTF8&condition=all"
          asin = "F018Y23P7K"
          calculate_asin = described_class.from_url(url)
          expect(calculate_asin).to eq asin
        end
      end

      context "/product-reviews/ or /offer-listing/" do
        it '/product-reviews/G018Y23P7K/' do
          url = "https://smile.amazon.com/product-reviews/G018Y23P7K/ref=acr_offerlistingpage_text?ie=UTF8&reviewerType=avp_only_reviews&showViewpoints=1"
          asin = "G018Y23P7K"
          calculate_asin = described_class.from_url(url)
          expect(calculate_asin).to eq asin
        end

        it '/questions/1764605/' do
          url = "http://stackoverflow.com/questions/1764605/scrape-asin-from-amazon-url-using-javascript"
          asin = nil
          calculate_asin = described_class.from_url(url)
          expect(calculate_asin).to eq asin
        end

        context "asin in query params" do
          it '?asin=H018Y23P7K' do
            url = "https://smile.amazon.com/forum/-/Tx3FTP6XCFXMJAO/ref=ask_dp_dpmw_al_hza?asin=H018Y23P7K"
            asin = "H018Y23P7K"
            calculate_asin = described_class.from_url(url)
            expect(calculate_asin).to eq asin
          end
          
          it '?ASIN=I018Y23P7K#R1VKN59YMEK5PC' do
            url = "https://smile.amazon.com/gp/customer-reviews/R1VKN59YMEK5PC/ref=cm_cr_arp_d_viewpnt?ie=UTF8&ASIN=I018Y23P7K#R1VKN59YMEK5PC"
            asin = "I018Y23P7K"
            calculate_asin = described_class.from_url(url)
            expect(calculate_asin).to eq asin
          end
        end   
      end
    end
end