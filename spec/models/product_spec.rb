require 'rails_helper'
require 'validate_url/rspec_matcher'

RSpec.describe Product, type: :model do
  subject{described_class.new(valid_params)}
  
  let(:valid_params){ {name: 'Amazon Echo', url: 'http://www.amazon.com', price: Money.new(100, 'EUR')} }
  
  it { is_expected.to validate_url_of(:url) }

  it "is valid with valid attributes" do
    is_expected.to be_valid
  end

  it "is not valid without a name" do
    subject.name = nil
    is_expected.to_not be_valid
  end
  
  describe "#url" do
    it "is not valid without a url" do
      subject.url = nil
      is_expected.to_not be_valid
    end
  
    it "is not valid without a url" do
      subject.url = 'amazon.com'
      is_expected.to_not be_valid
      expect(subject.errors.messages[:url]).to eq ['is not a valid URL']
    end
  end
end
