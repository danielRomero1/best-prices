FactoryBot.define do
  factory :product do
    name { "Fire tv stick" }
    url { "https://www.amazon.es/fire-tv-stick-con-mando-por-voz-alexa" }
    manufacturer { "Amazon" }
  end
end
