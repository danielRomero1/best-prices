class CreateProducts < ActiveRecord::Migration[7.0]
  def change
    create_table :products, id: :uuid do |t|
      t.string :asin
      t.string :name
      t.string :model
      t.string :manufacturer
      t.monetize :price
      t.string :url

      t.timestamps
    end
    add_index :products, :asin
  end
end
