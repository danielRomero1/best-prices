# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

This application was generated with this command: `rails new . -d postgresql -c tailwind`

```bash
docker run -d \
  --name postgres \
  -p 5432:5432 \
  -e POSTGRES_USER=postgres \
  -e POSTGRES_PASSWORD=password \
  -e PGDATA=/var/lib/postgresql/data/pgdata \
  -v ~/.docker_volumes/postgres:/var/lib/postgresql/data \
  postgres

NOTE: on `linux` add `--network=host` option to access the database via `127.0.0.1`. On `macos` and `windows` use the host: `localhost`
```